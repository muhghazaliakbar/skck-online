@extends('layouts.public')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center">FORMULIR PERMOHONAN SKCK ONLINE</h2>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" action="{{ route('register.create') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Biodata Pemohon</h4>
                                <div class="form-group">
                                    <label for="nik" class="col-md-3 control-label">NIK</label>
                                    <div class="col-md-9">
                                        <input type="number" class="form-control" id="nik" name="nik" placeholder="NIK">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama" class="col-sm-3 control-label">Nama Lengkap</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap">
                                        <p class="help-block">Nama lengkap sesuai KTP</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tempat_lahir" class="col-sm-3 control-label">Tempat Lahir</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat Lahir">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_lahir" class="col-sm-3 control-label">Tanggal Lahir</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="Tanggal Lahir">
                                        <p class="help-block">Format: tahun-bulan-tanggal (1993-10-16)</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="agama" class="col-sm-3 control-label">Agama</label>
                                    <div class="col-sm-9">
                                        <select name="agama" id="agama" class="form-control">
                                            @foreach(array_keys(\App\People::$agama) as $agama)
                                                <option value="{{ $agama }}">{{ ucfirst($agama) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="pekerjaan" class="col-sm-3 control-label">Pekerjaan</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" placeholder="Pekerjaan">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="alamat_sekarang" class="col-sm-3 control-label">Alamat Sekarang</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="alamat_sekarang" name="alamat_sekarang" placeholder="Alamat Sekarang">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jk" class="col-sm-3 control-label">Jenis Kelamin</label>
                                    <div class="col-sm-9">
                                        <select name="jk" id="jk" class="form-control">
                                            <option value="l">Laki-laki</option>
                                            <option value="p">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Dokumen</h4>
                                <div class="form-group">
                                    <label for="ktp" class="col-md-3 control-label">KTP</label>
                                    <div class="col-md-9">
                                        <input type="file" class="form-control" id="ktp" name="ktp" placeholder="KTP">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kk" class="col-md-3 control-label">Kartu keluarga</label>
                                    <div class="col-md-9">
                                        <input type="file" class="form-control" id="kk" name="kk" placeholder="Kartu keluarga">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-default">Submit</button>
                                        <a href="{{ url('/') }}" class="btn btn-default">Batal</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection