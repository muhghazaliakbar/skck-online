@extends('layouts.app')

@section('title')
    {{ $request->nama }} - {{ $request->nik }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <table class="table">
                <caption><b>Biodata</b></caption>
                <tr>
                    <td>#ID</td>
                    <td width="1%">:</td>
                    <td>{{ $request->id }}</td>
                </tr>
                <tr>
                    <td>NIK</td>
                    <td width="1%">:</td>
                    <td>{{ $request->nik }}</td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td width="1%">:</td>
                    <td>{{ $request->nama }}</td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td width="1%">:</td>
                    <td>{{ $request->gender }}</td>
                </tr>
                <tr>
                    <td>Tempat Lahir</td>
                    <td width="1%">:</td>
                    <td>{{ $request->tempat_lahir }}</td>
                </tr>
                <tr>
                    <td>Tanggal Lahir</td>
                    <td width="1%">:</td>
                    <td>{{ $request->birthday }}</td>
                </tr>
                <tr>
                    <td>Agama</td>
                    <td width="1%">:</td>
                    <td>{{ $request->religion }}</td>
                </tr>
                <tr>
                    <td>Pekerjaan</td>
                    <td width="1%">:</td>
                    <td>{{ $request->pekerjaan }}</td>
                </tr>
                <tr>
                    <td>Alamat Sekarang</td>
                    <td width="1%">:</td>
                    <td>{{ $request->alamat_sekarang }}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-6">
            <table class="table">
                <caption><b>Status</b></caption>
                <tr>
                    <td>Status</td>
                    <td width="1%">:</td>
                    <td>
                        @if($request->isExpired())
                            <span class="label label-danger">Kadaluarsa</span>
                        @else
                            <span class="label @if($request->isActive()) label-success @else label-default @endif">{{ $request->status }}</span>
                        @endif
                    </td>
                </tr>
                @if($request->isActive())
                    <tr>
                        <td>Nomor Registrasi</td>
                        <td width="1%">:</td>
                        <td>{{ $request->idReg }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Aktif</td>
                        <td width="1%">:</td>
                        <td>{{ $request->activeDate }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Kadaluarsa</td>
                        <td width="1%">:</td>
                        <td>{{ $request->expireDate }}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <caption><b>Pelanggaran</b></caption>
                <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Deskripsi</th>
                </tr>
                </thead>
                <tbody>
                @forelse($request->violation as $violation)
                    <tr>
                        <td>{{ $violation->date }}</td>
                        <td>{{ $violation->keterangan }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="2">Belum ada data.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection