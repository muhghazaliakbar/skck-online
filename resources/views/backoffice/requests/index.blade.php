@extends('layouts.app')

@section('title')
    Pemohon
@endsection

@section('content')
    <form action="">
        <input type="text" name="search" id="search" class="form-control" placeholder="Cari...">
    </form>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>#ID</th>
            <th>NIK</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>
                Tindakan
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($requests as $request)
            <tr class="@if($request->isActive()) success @endif @if($request->isExpired()) danger @endif">
                <td>{{ $request->id }}</td>
                <td>{{ $request->nik }}</td>
                <td>{{ $request->nama }}</td>
                <td>{{ $request->gender }}</td>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <a href="{{ route('request.show', $request->id) }}" class="btn btn-default" title="Lihat"><i class="glyphicon glyphicon-zoom-in"></i></a>
                        <button type="button" class="btn btn-default" title="Edit"><i class="glyphicon glyphicon-edit"></i></button>
                        <a href="{{ route('request.delete', $request->id) }}" class="btn btn-danger" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $requests->links() !!}
@endsection