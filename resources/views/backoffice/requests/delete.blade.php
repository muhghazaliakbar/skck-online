@extends('layouts.app')

@section('title')
    Konfirmasi Hapus Pemohon {{ $request->nama }} - {{ $request->nik }}
@endsection

@section('content')
    <form action="{{ route('request.destroy', $request->id) }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <h4>Apakah anda yakin ingin menghapus data pemohon ini?</h4>
        <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Hapus</button>
        <a href="{{ route('request.index') }}" class="btn btn-default">Batal</a>
    </form>
@endsection