@extends('layouts.app')

@section('title')
    Pelanggaran
@endsection

@section('content')
    <form action="">
        <input type="text" name="search" id="search" class="form-control" placeholder="Cari...">
    </form>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>#ID</th>
            <th>Tanggal</th>
            <th>NIK</th>
            <th>Nama</th>
            <th>
                Tindakan
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($violations as $violation)
            <tr>
                <td>{{ $violation->id }}</td>
                <td>{{ $violation->tanggal }}</td>
                <td>{{ $violation->nik }}</td>
                <td>{{ $violation->people->nama }}</td>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <a href="{{ route('request.show', $violation->id) }}" class="btn btn-default" title="Lihat"><i class="glyphicon glyphicon-zoom-in"></i></a>
                        <button type="button" class="btn btn-default" title="Edit"><i class="glyphicon glyphicon-edit"></i></button>
                        <a href="{{ route('request.delete', $violation->id) }}" class="btn btn-danger" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $violations->links() !!}
@endsection