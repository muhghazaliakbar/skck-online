<div class="col-md-3">
    <div class="panel panel-primary">
        <div class="panel-heading">Menu Utama</div>

        <!-- List group -->
        <ul class="list-group">
            <li class="list-group-item"><a href="{{ route('home') }}">Beranda</a></li>
            <li class="list-group-item"><a href="{{ route('request.index') }}">Pemohon</a></li>
            <li class="list-group-item"><a href="{{ route('violation.index') }}">Pelanggaran</a></li>
        </ul>
    </div>
</div>