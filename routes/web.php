<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');

/**
 * ==================================
 * Backoffice routes...
 * ==================================
 */

/**
 * Request routes...
 */
Route::get('pemohon', 'Backoffice\RequestController@index')->name('request.index');
Route::get('pemohon/{id}/show', 'Backoffice\RequestController@show')->name('request.show');
Route::get('pemohon/{id}/delete', 'Backoffice\RequestController@delete')->name('request.delete');
Route::delete('pemohon/{id}/destroy', 'Backoffice\RequestController@destroy')->name('request.destroy');

/**
 * Violation routes...
 */
Route::get('pelanggaran', 'Backoffice\ViolationController@index')->name('violation.index');

/**
 * ==================================
 * Public routes...
 * ==================================
 */
Route::get('daftar', 'RequestController@create')->name('register.create');
Route::post('daftar', 'RequestController@store')->name('register.store');