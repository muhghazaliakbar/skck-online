<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    /**
     * List Agama.
     *
     * @var array $agama
     */
    public static $agama = [
        'islam'     => 'Islam',
        'kristen'   => 'Kristen',
        'hindu'     => 'Hindu',
        'budha'     => 'Budha'
    ];

    /**
     * Jenis Kelamin.
     *
     * @var array $jk
     */
    public static $jk = [
        'l' => 'Laki-laki',
        'p' => 'Perempuan'
    ];
}
