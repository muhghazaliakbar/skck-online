<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Request extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $casts = [
        'is_active' => 'boolean'
    ];

    protected $dates = [
        'active_from',
        'active_to'
    ];

    protected $fillable = [
        'nik',
        'nama',
        'tempat_lahir',
        'tgl_lahir',
        'agama',
        'pekerjaan',
        'alamat_sekarang',
        'jk',
    ];

    /**
     * Jenis kelamin mutator.
     *
     * @return string
     */
    public function getGenderAttribute()
    {
        if ($this->jk === 'l') {
            return 'Laki-laki';
        } elseif ($this->jk === 'p') {
            return 'Perempuan';
        } else {
            return '';
        }
    }

    /**
     * Cek status aktif.
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->is_active;
    }

    /**
     * Cek status kadaluarsa.
     *
     * @return bool
     */
    public function isExpired()
    {
        if ($this->isActive()) {
            return $this->active_to < date('Y-m-d');
        }
    }

    /**
     * Status mutator.
     *
     * @return string
     */
    public function getStatusAttribute()
    {
        return $this->isActive() ? 'Aktif' : 'Tidak aktif';
    }

    /**
     * Format Tanggal lahir mutator.
     *
     * @return false|string
     */
    public function getBirthDayAttribute()
    {
        return date_format(date_create($this->tgl_lahir), 'd-M-Y');
    }

    /**
     * Agama Mutator.
     *
     * @return string
     */
    public function getReligionAttribute()
    {
        return ucfirst($this->agama);
    }

    /**
     * Tanggal Aktif Mutator.
     *
     * @return false|string
     */
    public function getActiveDateAttribute()
    {
        return date_format(date_create($this->active_from), 'd-M-Y');
    }

    /**
     * Tanggal Kadaluarsa Mutator.
     *
     * @return false|string
     */
    public function getExpireDateAttribute()
    {
        return date_format(date_create($this->active_to), 'd-M-Y');
    }

    /**
     * ID Registrasi mutator.
     *
     * @return string
     */
    public function getIdRegAttribute()
    {
        return strtoupper($this->reg_id);
    }

    public function violation()
    {
        return $this->hasMany(Violation::class, 'nik', 'nik');
    }
}
