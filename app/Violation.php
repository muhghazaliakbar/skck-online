<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Violation extends Model
{
    public function getDateAttribute()
    {
        return date_format(date_create($this->tanggal), 'd-M-Y');
    }

    public function people()
    {
        return $this->belongsTo(People::class, 'nik', 'nik');
    }
}
