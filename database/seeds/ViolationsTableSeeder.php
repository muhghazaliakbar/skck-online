<?php

use App\Request;
use App\Violation;
use Illuminate\Database\Seeder;

class ViolationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Violation::class, 10)->create();

        for ($i=1; $i<=15; $i++) {
            factory(Violation::class)->create(['nik' => factory(Request::class)->create()->nik]);
        }
    }
}
