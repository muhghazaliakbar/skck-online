<?php

use App\Request;
use Illuminate\Database\Seeder;

class RequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Request::class, 50)->create();

        factory(Request::class, 25)->states('active')->create();
    }
}
