<?php

use App\People;
use App\Violation;

$faker = Faker\Factory::create('id_ID');

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Violation::class, function () use ($faker) {

    return [
        'nik'           => factory(People::class)->create()->nik,
        'tanggal'       => $faker->date(),
        'keterangan'    => $faker->paragraph,
    ];
});
