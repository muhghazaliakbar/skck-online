<?php

use App\People;

$faker = Faker\Factory::create('id_ID');

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\People::class, function () use ($faker) {

    return [
        'nik'       => $faker->nik(),
        'nama'      => $faker->name,
        'tgl_lahir' => $faker->date('Y-m-d'),
        'jk'        => $faker->randomElement(array_keys(People::$jk)),
    ];
});
