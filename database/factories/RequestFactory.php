<?php

use App\People;
use Carbon\Carbon;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Request::class, function (Faker\Generator $faker) {
    $user = factory(People::class)->create();

    return [
        'nik'               => $user->nik,
        'nama'              => $user->nama,
        'tempat_lahir'      => $faker->text,
        'tgl_lahir'         => $user->tgl_lahir,
        'agama'             => $faker->randomElement(array_keys(People::$agama)),
        'pekerjaan'         => $faker->jobTitle,
        'alamat_sekarang'   => $faker->streetAddress,
        'jk'                => $faker->randomElement(array_keys(People::$jk)),
    ];
});

$factory->state(\App\Request::class, 'active', function (\Faker\Generator $faker) {
    $active = $faker->dateTimeThisYear->format('Y-m-d');

    return [
        'is_active'     => true,
        'reg_id'        => uniqid(),
        'active_from'   => $active,
        'active_to'     => Carbon::createFromFormat('Y-m-d', $active)->addMonth(6)
    ];
});
